Programming assignment unit 5
-----------------------------

Implement square root estimate and test.

Test script
-----------

./run.py (type to run test script, sample output out.txt)

Implementation
--------------

root.py (file that contains my_sqrt)

Test environment
----------------

OS lubuntu 16.04 lts 64bit kernel version 4.13.0 python version 2.7.12
