#!/usr/bin/python

"""
FILE run.py
"""

import math
from root import my_sqrt

def test_sqrt(mx):
    """
        method test_sqrt

        method used to test square root estimate

        parameters

        mx(integer) maximum value to test
    """
    rng = range(1,mx)
    diff = 1
    my = 1
    oth = 1
    for i in rng:
        my = my_sqrt(i)
        oth = math.sqrt(i)
        diff = abs(my - oth)
        print("a = {0} | my_sqrt(a) = {1} | math.sqrt(a) = {2} | diff = {3}".format(i,my,oth,diff))

if __name__ == "__main__":
    test_sqrt(26)
