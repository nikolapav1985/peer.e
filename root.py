#!/usr/bin/python

"""
FILE root.py
"""

def my_sqrt(a):
    """
        method my_sqrt

        estimate square root of a number

        parameters

        a(float) a number used to estimate square root

        return

        y(float) estimate of square root
    """
    x=a
    y=1
    while True:
        y=(x+a/float(x))/2.0 
        if y==x:
            break
        x=y
    return y
